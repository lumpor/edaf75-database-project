from bottle import get, post, run, request, response
import sqlite3
from sqlite3 import Error
import json
from datetime import date

host = "localhost"
port = 8888

try:
    conn = sqlite3.connect("cookiesDB.sqlite")
except Error as e:
    print(e)

def url(resource):
    return f"http://{HOST}:{PORT}{resource}"


def format_response(d):
    return json.dumps(d, indent=4)

def boolconv(b):
    if(b==1):
        return True
    return False

@get('/customers')
def get_customers():
    c = conn.cursor()
    c.execute(
        """
        SELECT *
        FROM   Customers
        """
    )
    s = [{"name": name, "address": address}
         for (name, address) in c]
    return format_response({"customers": s})

@post('/reset')
def reset():
    c = conn.cursor()
    c.executescript(
        """
        DELETE FROM Customers;
        DELETE FROM Orders;
        DELETE FROM Pallets;
        DELETE FROM Cookies;
        DELETE FROM Recipes;
        DELETE FROM Ingredients;
        DELETE FROM Restocks;
        DELETE FROM Order_details;
        INSERT INTO Customers VALUES ("Finkakor AB", "Helsingborg"), ("Småbröd AB", "Malmö"), ("Kaffebröd AB", "Landskrona"), ("Bjudkakor AB", "Ystad"), ("Kalaskakor AB", "Trelleborg"), ("Partykakor AB", "Kristianstad"), ("Gästkakor AB", "Hässleholm"), ("Skånekakor AB", "Perstorp");
        INSERT INTO Cookies VALUES ("Nut ring"), ("Nut cookie"), ("Amneris"), ("Tango"), ("Almond delight"), ("Berliner");
        INSERT INTO Ingredients VALUES ("Flour", "g"), ("Butter", "g"), ("Icing sugar", "g"), ("Roasted, chopped nuts", "g"), ("Fine-ground nuts", "g"), ("Ground, roasted nuts", "g"), ("Bread crumbs", "g"), ("Sugar", "g"), ("Egg whites", "ml"), ("Chocolate", "g"), ("Marzipan", "g"), ("Eggs", "g"), ("Potato starch", "g"), ("Wheat flour", "g"), ("Vanilla", "g"), ("Chopped almonds", "g"), ("Sodium bicarbonate", "g"), ("Cinnamon", "g"), ("Vanilla sugar", "g");
        INSERT INTO Recipes VALUES ("Nut ring", "Flour", 450), ("Nut ring", "Butter", 450), ("Nut ring", "Icing sugar", 190), ("Nut ring", "Roasted, chopped nuts", 225), ("Nut cookie", "Fine-ground nuts", 750), ("Nut cookie", "Ground, roasted nuts", 625), ("Nut cookie", "Bread crumbs", 125), ("Nut cookie", "Sugar", 375), ("Nut cookie", "Egg whites", 350), ("Nut cookie", "Chocolate", 50), ("Amneris", "Marzipan", 750), ("Amneris", "Butter", 250), ("Amneris", "Eggs", 250), ("Amneris", "Potato starch", 25), ("Amneris", "Wheat flour", 25), ("Tango", "Butter", 200), ("Tango", "Sugar", 250), ("Tango", "Flour", 300), ("Tango", "Sodium bicarbonate", 4), ("Tango", "Vanilla", 2), ("Almond delight", "Butter", 400), ("Almond delight", "Sugar", 270), ("Almond delight", "Chopped almonds", 279), ("Almond delight", "Flour", 400), ("Almond delight", "Cinnamon", 10), ("Berliner", "Flour", 350), ("Berliner", "Butter", 250), ("Berliner", "Icing sugar", 100), ("Berliner", "Eggs", 50), ("Berliner", "Vanilla sugar", 5), ("Berliner", "Chocolate", 50);
        """
    )
    c.execute(
        """
        INSERT INTO Restocks VALUES("Flour", ?, 100000), ("Butter", ?, 100000), ("Icing sugar", ?, 100000), ("Roasted, chopped nuts", ?, 100000), ("Fine-ground nuts", ?, 100000), ("Ground, roasted nuts", ?, 100000), ("Bread crumbs", ?, 100000), ("Sugar", ?, 100000), ("Egg whites", ?, 100000), ("Chocolate", ?, 100000), ("Marzipan", ?, 100000), ("Eggs", ?, 100000), ("Potato starch", ?, 100000), ("Wheat flour", ?, 100000), ("Sodium bicarbonate", ?, 100000), ("Vanilla", ?, 100000), ("Chopped almonds", ?, 100000), ("Cinnamon", ?, 100000), ("Vanilla sugar", ?, 100000);
        """,
        [date.today()] * 19
    )
    conn.commit()
    return format_response({"status": "ok"})


@get('/ingredients')
def get_ingredients():
    c = conn.cursor()
    c.execute(
        """
        SELECT   ingredient_name, sum(quantity), unit
        FROM     Ingredients
        JOIN     Restocks
        USING    (ingredient_name)
        GROUP BY (ingredient_name)
        """
    )
    s = [{"name": ingredient_name, "quantity": quantity, "unit": unit}
         for (ingredient_name, quantity, unit) in c]
    return format_response({"ingredients": s})

@get('/cookies')
def get_cookies():
    c = conn.cursor()
    c.execute(
        """
        SELECT cookie_name, 1
        FROM   Cookies
        """
    )
    s = [{"name": cookie_name}
         for (cookie_name, _) in c]
    #_ to make dict format work
    return format_response({"cookies": s})

@get('/recipes')
def get_recipes():
    c = conn.cursor()
    c.execute(
        """
        SELECT   cookie_name, ingredient_name, quantity, unit
        FROM     Recipes
        JOIN     Ingredients
        USING    (ingredient_name)
        """
    )
    s = [{"cookie": cookie_name, "ingredient": ingredient_name, "quantity": quantity, "unit": unit}
         for (cookie_name, ingredient_name, quantity, unit) in c]
    return format_response({"recipes": s})


     
@post('/pallets')
def postPallets():
    try:
        response.content_type = 'application/json'
        cookie = request.query.cookie
        if not (cookie):
            response.status = 401
            return 'Provide cookie name'
        c = conn.cursor()
        c.execute(
        """
        SELECT cookie_name
        FROM Cookies
        WHERE cookie_name = ?;
        """,
        [cookie]
        )
        bloj = c.fetchone()
        if (not bloj):
            response.status = 403
            return "no such cookie"
        c.execute(
            """
            INSERT INTO   Pallets(cookie_name, production_date)
            VALUES (?, ?)
            """,
            [cookie, date.today()]
        )
        conn.commit()
        c.execute(
            """
            SELECT   pallet_ID
            FROM     Pallets
            WHERE    rowid = last_insert_rowid()
            """
        )
        id = c.fetchone()[0]
        return format_response({"status": 'ok', "id": id})
    except Exception as e:
        return format_response({"status": str(e)})
        
@get('/pallets')
def getPallets():
    query =  """
             SELECT pallet_id, cookie_name, production_date, order_id, blocked
             FROM   Pallets
             WHERE  1=1
             """
    params = []
    if request.query.cookie:
        query += " AND cookie_name = ?"
        params.append(request.query.cookie)
    if request.query.blocked:
        query += " AND blocked = ?"
        params.append(request.query.blocked)
    if request.query.before:
        query += " AND production_date <= ?"
        params.append(request.query.before)
    if request.query.after:
        query += " AND production_date >= ?"
        params.append(request.query.after)
    c = conn.cursor()
    c.execute(
       query,
       params
    )
    conn.commit()
    
    s = [{"id": pallet_id, "cookie": cookie_name, "production_date": date, "customer": order_id, "blocked": boolconv(blocked)}
         for (pallet_id, cookie_name, date, order_id, blocked) in c]
    return format_response({"pallets": s})

@post('/block/<cookie>/<before>/<after>')
def block(cookie, before, after):
    
    c = conn.cursor()
    c.execute(
        """
        UPDATE Pallets 
        SET    blocked = 1
        WHERE cookie_name = ? AND production_date BETWEEN ? AND ?
        """,
        [cookie, before, after]
    )    
    return format_response({"status": 'ok'})

@post('/unblock/<cookie>/<before>/<after>')
def unblock(cookie, before, after):
    
    c = conn.cursor()
    c.execute(
        """
        UPDATE Pallets 
        SET    blocked = 0
        WHERE cookie_name = ? AND production_date BETWEEN ? AND ?
        """,
        [cookie, before, after]
    )    
    return format_response({"status": "ok"})

run(host=host, port=port)