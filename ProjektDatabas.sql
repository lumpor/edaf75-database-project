-- Delete the tables if they exist.
-- Disable foreign key checks, so the tables can
-- be dropped in arbitrary order.

PRAGMA foreign_keys=OFF;

DROP TABLE IF EXISTS Customers;
DROP TABLE IF EXISTS Orders;
DROP TABLE IF EXISTS Pallets;
DROP TABLE IF EXISTS Cookies;
DROP TABLE IF EXISTS Recipes;
DROP TABLE IF EXISTS Ingredients;
DROP TABLE IF EXISTS Restocks;
DROP TABLE IF EXISTS Order_details;

PRAGMA foreign_keys=ON;

-- Create the tables.
CREATE TABLE Customers(
	customer_name   TEXT,
	address  	TEXT,
	PRIMARY KEY (customer_name)
);

CREATE TABLE Orders(
	order_ID TEXT DEFAULT (lower(hex(randomblob(16)))),
	customer_name TEXT,
	delivery_date DATE,
	PRIMARY KEY   (order_ID)
	FOREIGN KEY   (customer_name) REFERENCES Customers(customer_name)
);

CREATE TABLE Order_details(
	order_id    TEXT,
	cookie_name TEXT,
	amount      INT,
	PRIMARY KEY (order_id, cookie_name)
	FOREIGN KEY (order_ID) REFERENCES Orders(order_ID)
	FOREIGN KEY (cookie_name) REFERENCES Cookies(cookie_name)
);

CREATE TABLE Pallets(
	pallet_ID       TEXT DEFAULT (lower(hex(randomblob(16)))),
	order_ID        TEXT DEFAULT NULL,
	cookie_name     TEXT,
	production_date DATE,
	blocked         BOOLEAN DEFAULT FALSE,
	PRIMARY KEY     (pallet_ID)
	FOREIGN KEY     (order_ID) REFERENCES Orders(order_ID)
	FOREIGN KEY     (cookie_name) REFERENCES Cookies(cookie_name)
);

CREATE TABLE Cookies(
	cookie_name TEXT,
	PRIMARY KEY (cookie_name)
);


CREATE TABLE Ingredients(
	ingredient_name TEXT,
	unit            TEXT,
	PRIMARY KEY     (ingredient_name)
);


CREATE TABLE Recipes(
	cookie_name     TEXT,
	ingredient_name TEXT,
	quantity        INT,
	PRIMARY KEY     (cookie_name, ingredient_name)
	FOREIGN KEY     (cookie_name) REFERENCES Cookies(cookie_name)
	FOREIGN KEY     (ingredient_name) REFERENCES Ingredients(ingredient_name)
);

CREATE TABLE Restocks(
	ingredient_name TEXT,
	date            DATE, 
	quantity        INT,
	FOREIGN KEY     (ingredient_name) REFERENCES Ingredients(ingredient_name)
);

-- Ingredient check before creating pallet, using event sourcing
DROP TRIGGER IF EXISTS missing_ingredient;
CREATE TRIGGER  missing_ingredient
BEFORE INSERT ON Pallets
BEGIN
  INSERT INTO	Restocks 	
     SELECT 	ingredient_name, new.production_date, -(Recipes.quantity*54)
     FROM 	Recipes
     WHERE 	Recipes.Cookie_name = new.Cookie_name;
   SELECT
    CASE WHEN
      (SELECT 		SUM(quantity)
       FROM   		Restocks
       WHERE  		ingredient_name IN 
      	 (SELECT 	ingredient_name
          FROM 		Recipes
          WHERE 	Recipes.Cookie_name = new.Cookie_name)
      	  GROUP BY      ingredient_name
       	  HAVING 	SUM(quantity) < 0)
          IS NOT	NULL   
    THEN
      RAISE 		(ROLLBACK, "not enough ingredients")
    END;

END;


