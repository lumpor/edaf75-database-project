# EDAF75, project report

This is the report for

 + Pham Minh-Vuong, soc13mph@student.lu.se
 + Friberg Fabian, dic13ffr@student.lu.se

Please ignore the empty folder in the project

We solved this project on our own, except for:

 + The Peer-review meeting


## ER-design

The model is in the file [`er-model.png`](er-model.png):

<center>
    <img src="er-model.png" width="100%">
</center>

(The image above describes the model from lecture 4, you
must replace the file '`er-model.png`' with an image of your
own ER-model).


## Relations

The ER-model above gives the following relations (neither
[Markdown](https://docs.gitlab.com/ee/user/markdown.html)
nor [HTML5](https://en.wikipedia.org/wiki/HTML5) handles
underlining withtout resorting to
[CSS](https://en.wikipedia.org/wiki/Cascading_Style_Sheets),
so we use bold face for primary keys, italicized face for
foreign keys, and bold italicized face for attributes which
are both primary keys and foreign keys):

+ customers(**customer_name**, address)
+ orders(**order_id**, _customer_name_, production_date, blocked)
+ pallets(**pallet_id**, _order_id_, _cookie_name_, production_date)
+ cookies(**cookie_name**)
+ recipes(**_cookie_name_**, **_ingredient_name_**, quantity)
+ ingredients(**ingredient_name**, unit)
+ restocks(_ingredient_name_, date, quantity)



## Scripts to set up database

The scripts used to set up and populate the database are in:

 + [`ProjektDatabas.sql`](ProjektDatabas.sql) (defines the tables)
 + Use the "/reset" command in the API (insert values into the tables)

So, to create and initialize the database, we run:

```shell
sqlite3 cookiesDB.sqlite < ProjektDatabas.sql

```

## How to compile and run the program

This section should give a few simple commands to type to
compile and run the program from the command line, such as:

```shell
python ./cookiesAPI.py
python ./check-krusty.py
```